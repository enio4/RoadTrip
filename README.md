
# Trip Planner Application

## Description

Trip Planner Application est une application web permettant de planifier et visualiser des voyages sur une carte interactive. L'application permet d'ajouter, de modifier et de supprimer des lieux de voyage, ainsi que de générer des liens partageables pour vos itinéraires.

## Fonctionnalités

- Ajouter, modifier et supprimer des lieux de voyage.
- Afficher les lieux de voyage sur une carte interactive.
- Générer des liens partageables pour les itinéraires.
- Interface utilisateur responsive avec des boutons d'expansion pour mobile.
- Sauvegarde automatique des données de voyage dans le stockage local du navigateur.

## Prérequis

Assurez-vous d'avoir les éléments suivants installés sur votre machine :

- Node.js (version 14 ou supérieure)
- npm (version 6 ou supérieure) ou yarn (version 1.22 ou supérieure)

## Installation

### Cloner le dépôt

```bash
git clone https://gitlab.com/enio4/RoadTrip
cd trip-planner
```

### Installer les dépendances

Utilisez npm ou yarn pour installer les dépendances :

```bash
npm install
```

ou

```bash
yarn install
```

### Lancer l'application en local

Après avoir configuré votre clé API, lancez l'application en utilisant npm ou yarn :

```bash
npm start
```

ou

```bash
yarn start
```

L'application sera accessible à l'adresse suivante : [http://localhost:3000](http://localhost:3000)

## Dépendances

- React
- React-DOM
- React-Google-Maps
- React-Modal
- React-Calendar
- Material-UI
- uuid
- Leaflet