import React, { useState, useRef, useEffect } from 'react';
import { v4 as uuidv4 } from 'uuid';
import MapViewer from './components/MapViewer/MapViewer';
import TripView from './components/LeftSideBar/TripView';
import ModifyTripModal from './components/Utils/ModifyTripModal';
import TripInfo from './components/RightSideBar/TripInfo';
import WelcomeAnimation from './components/Utils/WelcomeAnimation';
import { haversineDistance, determineTransportMode } from './components/Utils/transportmode';
import Modal from 'react-modal';
import { Button } from '@mui/material';
import './styles/App.css';

Modal.setAppElement('#root');

function App() {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [modalTrip, setModalTrip] = useState(null);
  const [modalType, setModalType] = useState(null);
  const [trips, setTrips] = useState([]);
  const [mapCenter, setMapCenter] = useState({ lat: 45.759529, lng: 4.861140 });
  const [expandedSection, setExpandedSection] = useState(null);
  const [showWelcome, setShowWelcome] = useState(false);
  const mapRef = useRef(null);

  const debounceTimeout = useRef(null);

  useEffect(() => {
    const savedTrips = localStorage.getItem('trips');
    if (savedTrips) {
      setTrips(JSON.parse(savedTrips));
    }

    const isFirstVisit = localStorage.getItem('isFirstVisit');
    if (!isFirstVisit) {
      setShowWelcome(true);
      localStorage.setItem('isFirstVisit', 'false');
    }
  }, []);

  useEffect(() => {
    localStorage.setItem('trips', JSON.stringify(trips));
  }, [trips]);

  const openModal = (trip = null) => {
    if (trips.length === 0) {
      setModalType('start');
    } else if (trips.length === 1) {
      setModalType('end');
    } else {
      setModalType('intermediate');
    }
    setModalTrip(trip);
    setModalIsOpen(true);
  };

  const closeModal = () => {
    setModalIsOpen(false);
    setModalTrip(null);
    setModalType(null);
  };

  const sortTripsByDate = (trips) => {
    return trips.slice().sort((a, b) => new Date(a.startDate) - new Date(b.startDate));
  };

  const updateTripTypesAndDates = (updatedTrips) => {
    if (updatedTrips.length === 0) {
      setTrips([]);
      return;
    }

    if (updatedTrips.length === 1) {
      updatedTrips[0].type = 'start';
      updatedTrips[0].color = 'green';
    } else {
      updatedTrips[0].type = 'start';
      updatedTrips[0].color = 'green';
      updatedTrips[updatedTrips.length - 1].type = 'end';
      updatedTrips[updatedTrips.length - 1].color = 'red';
      for (let i = 1; i < updatedTrips.length - 1; i++) {
        updatedTrips[i].type = 'intermediate';
        updatedTrips[i].color = getRandomColor();
      }
    }
    setTrips(updatedTrips);
  };

  const getRandomColor = () => {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  };

  const addTrip = (trip) => {
    const newTrip = { ...trip, id: uuidv4(), type: modalType || 'intermediate', color: modalType === 'intermediate' ? getRandomColor() : undefined };
    if (trips.length > 0) {
      const lastTrip = trips[trips.length - 1];
      const distance = haversineDistance(lastTrip.lat, lastTrip.lng, trip.lat, trip.lng);
      newTrip.transportMode = determineTransportMode(distance);
    } else {
      newTrip.transportMode = 'driving';
    }
    const updatedTrips = sortTripsByDate([...trips, newTrip]);
    updateTripTypesAndDates(updatedTrips);
    setMapCenter({ lat: trip.lat, lng: trip.lng });
  };

  const updateTrip = (updatedTrip) => {
    const updatedTrips = trips.map(trip => (trip.id === updatedTrip.id ? { ...trip, ...updatedTrip } : trip));
    const sortedTrips = sortTripsByDate(updatedTrips);
    updateTripTypesAndDates(sortedTrips);
  };

  const deleteTrip = (tripId) => {
    const updatedTrips = trips.filter(trip => trip.id !== tripId);
    const sortedTrips = sortTripsByDate(updatedTrips);
    updateTripTypesAndDates(sortedTrips);
  };

  const setMapCenterHandler = (lat, lng) => {
    if (lat !== undefined && lng !== undefined && (lat !== mapCenter.lat || lng !== mapCenter.lng)) {
      if (debounceTimeout.current) {
        clearTimeout(debounceTimeout.current);
      }
      debounceTimeout.current = setTimeout(() => {
        setMapCenter({ lat, lng });
      }, 1000); // Ajustez le délai selon vos besoins
    }
  };

  const handleDestinationClick = (destination) => {
    const trip = {
      name: destination.name,
      lat: destination.lat,
      lng: destination.lng,
      startDate: new Date().toISOString(),
      endDate: new Date().toISOString(),
      transportMode: 'driving'
    };
    openModal(trip);
  };

  const generateShareableLink = () => {
    const tripsData = JSON.stringify(trips);
    const encodedTrips = encodeURIComponent(tripsData);
    const baseUrl = window.location.origin;
    const shareableLink = `${baseUrl}/shared/${encodedTrips}`;
    return shareableLink;
  };

  const handleShare = () => {
    const shareableLink = generateShareableLink();
    navigator.clipboard.writeText(shareableLink).then(() => {
      alert("Trip link copied to clipboard!");
    }, (err) => {
      console.error('Failed to copy link: ', err);
    });
  };

  const toggleExpandSection = (section) => {
    const newExpandedSection = expandedSection === section ? null : section;
    setExpandedSection(newExpandedSection);
  };

  const handlePlaceClick = (lat, lng) => {
    setMapCenter({ lat, lng });
    if (mapRef.current) {
      mapRef.current.flyTo([lat, lng], 14); // Ajustez le niveau de zoom selon vos besoins
    }
  };

  return (
    <div className={`App ${expandedSection ? `expanded-${expandedSection}` : ''}`}>
      {showWelcome && <WelcomeAnimation onClose={() => setShowWelcome(false)} />}
      <div className="sidebar">
        <TripView
          trips={trips}
          onOpenModal={openModal}
          onPlaceClick={handlePlaceClick} // Passer la fonction de recentrage
          onDeleteTrip={deleteTrip}
        />
        <div className={`expand-button expand-button-top ${expandedSection ? `slide-down` : ''}`} onClick={() => toggleExpandSection('tripView')}>
          {expandedSection === 'tripView' ? 'Minimize' : 'Expand'}
        </div>
      </div>
      <div className="mapContainer">
        <MapViewer places={trips} mapCenter={mapCenter} onMapCenterChanged={setMapCenterHandler} mapRef={mapRef} />
        <Button variant="contained" color="primary" className="shareable-link-button" onClick={handleShare}>Copy Shareable Link</Button>
      </div>
      <div className="tripInfo">
        <div className={`expand-button expand-button-bottom ${expandedSection ? `slide-up` : ''}`} onClick={() => toggleExpandSection('tripInfo')}>
          {expandedSection === 'tripInfo' ? 'Minimize' : 'Expand'}
        </div>
        <TripInfo trips={trips} onDestinationClick={handleDestinationClick} mapCenter={mapCenter} />
      </div>
      <ModifyTripModal
        isOpen={modalIsOpen}
        onClose={closeModal}
        onAddTrip={modalTrip && modalTrip.id ? updateTrip : addTrip}
        onAddPlace={setMapCenterHandler}
        trip={modalTrip}
        modalType={modalType}
        existingTrips={trips}
      />
    </div>
  );
}

export default App;
