import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import SharedTrip from './components/Utils/SharedTrip';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import reportWebVitals from './reportWebVitals';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';

const theme = createTheme({
    palette: {
        primary: {
            main: '#007bff',
        },
        background: {
            default: '#f0f0f0',
        },
    },
    typography: {
        fontFamily: 'Arial, sans-serif',
    },
});

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <ThemeProvider theme={theme}>
        <CssBaseline />
        <Router>
            <Routes>
                <Route path="/shared/:encodedTrips" element={<SharedTrip />} />
                <Route path="/" element={<App />} />
            </Routes>
        </Router>
    </ThemeProvider>
);

reportWebVitals();
