import React, { useEffect, useRef } from 'react';
import L from 'leaflet';
import { startIcon, endIcon, createNumberedIcon } from '../Utils/leafleticon';
import { haversineDistance, determineTransportMode } from '../Utils/transportmode';

import 'leaflet/dist/leaflet.css';
import '../../styles/MapViewer/MapViewer.css';

const mapboxToken = 'pk.eyJ1IjoiZW5pb3NhZiIsImEiOiJjbHd0czBtd2MwNnJ0MmxxbDV5dmVqZnoyIn0.oq4Tzt7SRDIlbpdlD-E2jA';

function MapViewer({ places, mapCenter, onMapCenterChanged }) {
    const mapRef = useRef(null);
    const mapContainerRef = useRef(null);
    const markersRef = useRef([]);
    const polylineRefs = useRef([]);

    const debounceRef = useRef(null);

    useEffect(() => {
        if (!mapRef.current && mapCenter.lat !== undefined && mapCenter.lng !== undefined) {
            mapRef.current = L.map(mapContainerRef.current).setView([mapCenter.lat, mapCenter.lng], 13);
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '© OpenStreetMap contributors'
            }).addTo(mapRef.current);

            mapRef.current.on('moveend', () => {
                if (debounceRef.current) {
                    clearTimeout(debounceRef.current);
                }
                debounceRef.current = setTimeout(() => {
                    if (onMapCenterChanged && mapRef.current) {
                        const center = mapRef.current.getCenter();
                        onMapCenterChanged(center.lat, center.lng);
                    }
                }, 1500); // Adjust debounce delay as needed
            });
        }
    }, [mapCenter, onMapCenterChanged]);

    useEffect(() => {
        if (mapRef.current) {
            markersRef.current.forEach(marker => marker.remove());
            markersRef.current = places.map((place, index) => {
                if (place.lat !== undefined && place.lng !== undefined) {
                    let marker;
                    if (place.type === 'start') {
                        marker = L.marker([place.lat, place.lng], { icon: startIcon });
                    } else if (place.type === 'end') {
                        marker = L.marker([place.lat, place.lng], { icon: endIcon });
                    } else {
                        marker = L.marker([place.lat, place.lng], { icon: createNumberedIcon(index + 1) });
                    }
                    return marker.addTo(mapRef.current).bindPopup(`${place.name} (${place.transportMode})`);
                }
                return null;
            }).filter(marker => marker !== null);

            polylineRefs.current.forEach(polyline => polyline.remove());
            polylineRefs.current = [];

            if (places.length > 1) {
                for (let i = 0; i < places.length - 1; i++) {
                    const from = places[i];
                    const to = places[i + 1];
                    const distance = haversineDistance(from.lat, from.lng, to.lat, to.lng);
                    const mode = determineTransportMode(distance);

                    if (mode === 'flight') {
                        const latlngs = [
                            [from.lat, from.lng],
                            [to.lat, to.lng]
                        ];
                        const polyline = L.polyline(latlngs, { color: 'red', dashArray: '5, 10' }).addTo(mapRef.current);
                        polylineRefs.current.push(polyline);
                    } else {
                        const url = `https://api.mapbox.com/directions/v5/mapbox/${mode}/${from.lng},${from.lat};${to.lng},${to.lat}?geometries=geojson&overview=full&access_token=${mapboxToken}`;

                        fetch(url)
                            .then(response => {
                                if (!response.ok) {
                                    throw new Error(`HTTP error! status: ${response.status}`);
                                }
                                return response.json();
                            })
                            .then(data => {
                                if (data.routes && data.routes.length > 0) {
                                    const route = data.routes[0].geometry.coordinates;
                                    const latlngs = route.map(coord => [coord[1], coord[0]]);
                                    const polyline = L.polyline(latlngs, { color: 'blue', smoothFactor: 1 }).addTo(mapRef.current);
                                    polylineRefs.current.push(polyline);
                                }
                            })
                            .catch(error => console.error('Error fetching route:', error));
                    }
                }
            }
        }
    }, [places]);

    useEffect(() => {
        if (mapRef.current && mapCenter.lat !== undefined && mapCenter.lng !== undefined) {
            mapRef.current.setView(new L.LatLng(mapCenter.lat, mapCenter.lng), mapRef.current.getZoom());
        }
    }, [mapCenter]);

    return <div ref={mapContainerRef} className="map" style={{ height: '100%' }}></div>;
}

export default MapViewer;
