import React, { useEffect, useState } from 'react';
import { Button, Typography, Box } from '@mui/material';
import TripCalendar from '../Utils/TripCalendar';
import PopularDestinations from '../Utils/PopularDestinations';
import '../../styles/RightSideBar/TripInfo.css';

const TripInfo = ({ trips, onDestinationClick, readOnly, mapCenter }) => {
    const [tripSummary, setTripSummary] = useState("Hey guys! I thought of a really cool trip to do together.");

    useEffect(() => {
        if (trips.length > 0) {
            let summary = "Hey guys ! I thought of a really cool trip to do together.\n";
            const startTrip = trips.find(trip => trip.type === 'start');
            const endTrip = trips.find(trip => trip.type === 'end');
            const intermediateTrips = trips.filter(trip => trip.type === 'intermediate');

            const formatDateRange = (startDate, endDate, color) => {
                const start = new Date(startDate).toLocaleDateString('fr-FR');
                const end = new Date(endDate).toLocaleDateString('fr-FR');
                return start === end ? `the <span style="color: ${color};">${start}</span>` : `from <span style="color: ${color};">${start}</span> to <span style="color: ${color};">${end}</span>`;
            };

            const formatPlace = (index, place) => {
                const { name } = place;
                return `${name}`;
            };

            if (startTrip) {
                const color = startTrip.color;
                summary += `We could start at <span style="color: ${color};">${formatPlace(0, startTrip)}</span> ${formatDateRange(startTrip.startDate, startTrip.endDate, color)}.\n`;
            }

            intermediateTrips.forEach((trip, index) => {
                const color = trip.color;
                summary += `Then we could go to <span style="color: ${color};">${formatPlace(index + 1, trip)}</span> ${formatDateRange(trip.startDate, trip.endDate, color)}.\n`;
            });

            if (endTrip) {
                const color = endTrip.color;
                summary += `Finally, we would arrive at <span style="color: ${color};">${formatPlace(intermediateTrips.length + 1, endTrip)}</span> ${formatDateRange(endTrip.startDate, endTrip.endDate, color)}\n`;
            }

            setTripSummary(summary);
        } else {
            setTripSummary("Hey guys! I thought of a really cool trip to do together.");
        }
    }, [trips]);

    const handleCopy = () => {
        const textToCopy = tripSummary.replace(/<[^>]*>/g, ''); // Removing HTML tags for clipboard copy
        navigator.clipboard.writeText(textToCopy).then(() => {
            alert("Trip summary copied to clipboard!");
        }, (err) => {
            console.error('Failed to copy text: ', err);
        });
    };

    return (
        <Box className="trip-info">
            <Typography variant="h5">Global Trip Calendar</Typography>
            <TripCalendar trips={trips} />

            <Box className="trip-summary-section">
            <Typography variant="h5">Trip Summary</Typography>
                <div dangerouslySetInnerHTML={{ __html: tripSummary }}></div>
                <Button variant="contained" color="primary" onClick={handleCopy}>Copy Trip Summary</Button>
            </Box>

            {!readOnly && <PopularDestinations onDestinationClick={onDestinationClick} mapCenter={mapCenter} />}
        </Box>
    );
};

export default TripInfo;
