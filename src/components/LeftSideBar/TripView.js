import React from 'react';
import { Button, Card, CardContent, Typography } from '@mui/material';
import '../../styles/LeftSideBar/TripView.css';

function TripView({ trips, onOpenModal, onPlaceClick, onDeleteTrip, readOnly }) {
    const formatDateRange = (startDate, endDate) => {
        const start = new Date(startDate).toLocaleDateString('fr-FR');
        const end = new Date(endDate).toLocaleDateString('fr-FR');
        return start === end ? `${start}` : `from ${start} to ${end}`;
    };

    const getTransportEmoji = (transportMode) => {
        switch (transportMode) {
            case 'driving':
                return '🚗';
            case 'flight':
                return '✈️';
            default:
                return '';
        }
    };

    return (
        <div>
            {!readOnly && (
                <Button className='button-add' variant="contained" color="primary" onClick={() => onOpenModal()}>
                    {trips.length > 0 ? 'Add a new place!' : 'Plan your next trip!'}
                </Button>
            )}
            <div className="trip-list">
                {trips.map((trip, index) => (
                    <Card key={trip.id} className="trip" style={{ borderLeft: `5px solid ${trip.color}` }}>
                        <CardContent>
                            {trip.type === 'start' && <Typography variant="caption" className="trip-label">Start</Typography>}
                            {trip.type === 'end' && <Typography variant="caption" className="trip-label">End</Typography>}
                            <Typography variant="h6" onClick={() => !readOnly && onPlaceClick(trip.lat, trip.lng)}>
                                {`${index + 1}. ${trip.name} ${getTransportEmoji(trip.transportMode)}`}
                            </Typography>
                            <Typography variant="body2">Date: {formatDateRange(trip.startDate, trip.endDate)}</Typography>
                            {!readOnly && (
                                <div className='button-tripview'>
                                    <Button className="edit-button" variant="outlined" color="primary" onClick={() => onOpenModal(trip)}>Edit</Button>
                                    <Button className="delete-button" variant="outlined" color="secondary" onClick={() => onDeleteTrip(trip.id)}>Delete</Button>
                                </div>
                            )}
                        </CardContent>
                    </Card>
                ))}
            </div>
        </div>
    );
}

export default TripView;
