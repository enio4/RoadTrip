import React from 'react';
import Calendar from 'react-calendar';

import 'react-calendar/dist/Calendar.css';

const TripCalendar = ({ trips }) => {
    const tileContent = ({ date, view }) => {
        if (view === 'month') {
            return trips.map((trip) => {
                const tripDates = [];
                const startDate = new Date(trip.startDate);
                const endDate = new Date(trip.endDate);
                for (let d = new Date(startDate); d <= endDate; d.setDate(d.getDate() + 1)) {
                    tripDates.push(new Date(d).toDateString());
                }
                if (tripDates.includes(date.toDateString())) {
                    return (
                        <div
                            key={trip.id}
                            style={{
                                backgroundColor: trip.color,
                                width: '10px',
                                height: '10px',
                                borderRadius: '50%'
                            }}
                        ></div>
                    );
                }
                return null;
            });
        }
        return null;
    };

    return (
        <Calendar
            tileContent={tileContent}
            locale="fr-FR"
            formatShortWeekday={(locale, date) => {
                // Custom function to force Monday as the first day
                const weekdays = ['Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim'];
                return weekdays[date.getDay()];
            }}
        />
    );
};

export default TripCalendar;
