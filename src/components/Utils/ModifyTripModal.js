import React, { useState, useRef, useEffect } from 'react';
import ReactModal from 'react-modal';
import { useLoadScript, Autocomplete } from '@react-google-maps/api';
import Calendar from 'react-calendar';
import { Button } from '@mui/material';

import 'react-calendar/dist/Calendar.css';
import '../../styles/Utils/ModifyTripModal.css';

const libraries = ["places"];
const googleMapsApiKey = "AIzaSyC25gLCtktW1RDNciVYIA2ChBksCOccCwY";

function ModifyTripModal({ isOpen, onClose, onAddTrip, onAddPlace, trip, existingTrips, modalType }) {
    const [name, setName] = useState('');
    const [dateRange, setDateRange] = useState([new Date(), new Date()]);
    const [placeDetails, setPlaceDetails] = useState(null);
    const autocompleteRef = useRef(null);

    useEffect(() => {
        if (trip) {
            setName(trip.name);
            setDateRange([new Date(trip.startDate), new Date(trip.endDate)]);
            setPlaceDetails({ name: trip.name, lat: trip.lat, lng: trip.lng });
        } else {
            setName('');
            setDateRange([new Date(), new Date()]);
            setPlaceDetails(null);
        }
    }, [trip]);

    const { isLoaded } = useLoadScript({
        googleMapsApiKey: googleMapsApiKey,
        libraries: libraries,
    });

    const handleSubmit = (event) => {
        event.preventDefault();
        if (!placeDetails || !placeDetails.lat || !placeDetails.lng) {
            alert("Please select a valid location.");
            return;
        }
        const newTrip = {
            ...trip, // Keep the id and other properties if editing an existing trip
            name: placeDetails.name,
            startDate: dateRange[0].toISOString(),
            endDate: dateRange[1].toISOString(),
            lat: placeDetails.lat,
            lng: placeDetails.lng,
            color: trip && trip.color ? trip.color : (modalType === 'intermediate' ? getRandomColor() : undefined)
        };
        onAddTrip(newTrip);
        onClose();
    };

    const getRandomColor = () => {
        const letters = '0123456789ABCDEF';
        let color = '#';
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    };

    const tileContent = ({ date, view }) => {
        if (view === 'month') {
            return existingTrips.map((trip) => {
                const tripDates = [];
                const startDate = new Date(trip.startDate);
                const endDate = new Date(trip.endDate);
                for (let d = new Date(startDate); d <= endDate; d.setDate(d.getDate() + 1)) {
                    tripDates.push(new Date(d).toDateString());
                }
                if (tripDates.includes(date.toDateString())) {
                    return (
                        <div
                            key={trip.id}
                            style={{
                                backgroundColor: trip.color,
                                width: '10px',
                                height: '10px',
                                borderRadius: '50%'
                            }}
                        ></div>
                    );
                }
                return null;
            });
        }
        return null;
    };

    const modalTitle = modalType === 'start' ? 'Tell us your Start Location!' : modalType === 'end' ? 'Tell us your End Location!' : 'Modify Trip';

    if (!isLoaded) return <div>Loading...</div>;

    return (
        <ReactModal
            isOpen={isOpen}
            onRequestClose={onClose}
            className={{
                base: 'modalContent',
                afterOpen: 'modalContent_after-open',
                beforeClose: 'modalContent_before-close'
            }}
            overlayClassName={{
                base: 'overlay',
                afterOpen: 'overlay_after-open',
                beforeClose: 'overlay_before-close'
            }}
        >
            <h2>{modalTitle}</h2>
            <form className='modalform' onSubmit={handleSubmit}>
                <label htmlFor="name">Where do you want to go?</label>
                <Autocomplete
                    onLoad={(autocomplete) => autocompleteRef.current = autocomplete}
                    onPlaceChanged={() => {
                        if (autocompleteRef.current !== null) {
                            const place = autocompleteRef.current.getPlace();
                            if (place.geometry) {
                                const formattedAddress = place.formatted_address;
                                const lat = place.geometry.location.lat();
                                const lng = place.geometry.location.lng();
                                setPlaceDetails({
                                    name: formattedAddress,
                                    lat: lat,
                                    lng: lng,
                                });
                                setName(formattedAddress);
                                onAddPlace(lat, lng);
                            } else {
                                console.error("Place geometry is undefined");
                            }
                        }
                    }}
                >
                    <input
                        id="name"
                        type="text"
                        value={name}
                        onChange={e => setName(e.target.value)}
                    />
                </Autocomplete>
                <label htmlFor="date">When?</label>
                <Calendar
                    className={'modalcalendar'}
                    selectRange={true}
                    onChange={setDateRange}
                    value={dateRange}
                    tileContent={tileContent}
                    locale="fr-FR"
                />
                <Button variant="contained" color="primary" type="submit">Let's go!</Button>
                <Button variant="outlined" color="secondary" onClick={onClose}>Cancel</Button>
            </form>
        </ReactModal>
    );
}

export default ModifyTripModal;
