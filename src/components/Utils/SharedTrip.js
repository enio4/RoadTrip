import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import MapViewer from '../MapViewer/MapViewer';
import TripView from '../LeftSideBar/TripView';
import TripInfo from '../RightSideBar/TripInfo';

function SharedTrip() {
  const { encodedTrips } = useParams();
  const [trips, setTrips] = useState([]);

  useEffect(() => {
    if (encodedTrips) {
      const decodedTrips = decodeURIComponent(encodedTrips);
      const tripsData = JSON.parse(decodedTrips);
      setTrips(tripsData);
    }
  }, [encodedTrips]);

  return (
    <div className="App">
      <div className="sidebar">
        <TripView trips={trips} readOnly={true} />
      </div>
      <div className="mapContainer">
        <MapViewer places={trips} mapCenter={trips.length > 0 ? { lat: trips[0].lat, lng: trips[0].lng } : { lat: 45.759529, lng: 4.861140 }} />
      </div>
      <div className="tripInfo">
        <TripInfo trips={trips} readOnly={true} />
      </div>
    </div>
  );
}

export default SharedTrip;
