import React, { useEffect } from 'react';
import '../../styles/Utils/WelcomeAnimation.css';

function WelcomeAnimation({ onClose }) {
    useEffect(() => {
        const timer = setTimeout(() => {
            onClose();
        }, 4000); // Durée de l'animation

        return () => clearTimeout(timer);
    }, [onClose]);

    return (
        <div className="welcome-container">
            <div className="logo-container">
                <img src="/RoadTripLogo/roadtriplogo.png" alt="Logo" className="logo" />
                <p className="welcome-text">Welcome to the</p>
                <p className="brand-text">Flower Trip Planner ©</p>
            </div>
        </div>
    );
}

export default WelcomeAnimation;
