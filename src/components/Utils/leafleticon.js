import L from 'leaflet';
import iconRetinaUrl from 'leaflet/dist/images/marker-icon-2x.png';
import iconUrl from 'leaflet/dist/images/marker-icon.png';
import shadowUrl from 'leaflet/dist/images/marker-shadow.png';

const startIconUrl = '/leafleticons/start-marker.png';
const endIconUrl = '/leafleticons/end-marker.png';

const defaultIcon = L.icon({
    iconRetinaUrl,
    iconUrl,
    shadowUrl,
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    tooltipAnchor: [16, -28],
    shadowSize: [41, 41]
});

const startIcon = L.icon({
    iconUrl: startIconUrl,
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    tooltipAnchor: [16, -28],
    shadowSize: [41, 41]
});

const endIcon = L.icon({
    iconUrl: endIconUrl,
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    tooltipAnchor: [16, -28],
    shadowSize: [41, 41]
});

function createNumberedIcon(number) {
    return L.divIcon({
        className: 'numbered-icon',
        html: `<div class="icon-wrapper">
                  <img src="${iconUrl}" class="icon-img" />
                  <span class="icon-number">${number}</span>
               </div>`,
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        tooltipAnchor: [16, -28]
    });
}

L.Marker.prototype.options.icon = defaultIcon;

export { defaultIcon, startIcon, endIcon, createNumberedIcon };
