import React, { useState, useEffect, useRef } from 'react';
import { useLoadScript } from '@react-google-maps/api';
import { Button, Typography, List, ListItem, Box } from '@mui/material';
import '../../styles/Utils/PopularDestinations.css';

const libraries = ["places"];
const googleMapsApiKey = "AIzaSyC25gLCtktW1RDNciVYIA2ChBksCOccCwY";

const PopularDestinations = ({ onDestinationClick, mapCenter }) => {
    const [expandedId, setExpandedId] = useState(null);
    const [popularDestinations, setPopularDestinations] = useState([]);
    const [visibleCount, setVisibleCount] = useState(5);
    const { isLoaded, loadError } = useLoadScript({
        googleMapsApiKey: googleMapsApiKey,
        libraries: libraries,
    });

    const debounceTimeout = useRef(null);

    useEffect(() => {
        if (isLoaded && mapCenter.lat !== undefined && mapCenter.lng !== undefined) {
            if (debounceTimeout.current) {
                clearTimeout(debounceTimeout.current);
            }
            debounceTimeout.current = setTimeout(() => {
                const service = new window.google.maps.places.PlacesService(document.createElement('div'));
                const request = {
                    location: new window.google.maps.LatLng(mapCenter.lat, mapCenter.lng),
                    radius: 10000,
                    type: ['tourist_attraction'],
                };

                service.nearbySearch(request, (results, status) => {
                    if (status === window.google.maps.places.PlacesServiceStatus.OK) {
                        const destinations = results.map(result => ({
                            id: result.place_id,
                            name: result.name,
                            formatted_address: result.vicinity,
                            lat: result.geometry.location.lat(),
                            lng: result.geometry.location.lng(),
                        }));
                        setPopularDestinations(destinations);
                    }
                });
            }, 1000); // Ajustez le délai selon vos besoins
        }
    }, [isLoaded, mapCenter]);

    const handleExpandClick = (id) => {
        setExpandedId(expandedId === id ? null : id);
    };

    const handleMoreInfoClick = (name) => {
        window.open(`https://www.google.com/search?q=${name}`, '_blank');
    };

    const showMoreDestinations = () => {
        setVisibleCount(prevCount => prevCount + 5);
    };

    const showLessDestinations = () => {
        setVisibleCount(5);
    };

    if (loadError) {
        return <div>Error loading Google Maps API</div>;
    }

    if (!isLoaded) {
        return <div>Loading...</div>;
    }

    return (
        <div className="popular-destinations">
            <Typography variant="h5">Popular Destinations</Typography>
            <List>
                {popularDestinations.slice(0, visibleCount).map(destination => (
                    <ListItem key={destination.id} disablePadding>
                        <Box width="100%">
                            <Typography variant="body1" onClick={() => handleExpandClick(destination.id)}>
                                {destination.name}
                            </Typography>
                            {expandedId === destination.id && (
                                <Box className="destination-details">
                                    <Button variant="text" color="primary" onClick={() => handleMoreInfoClick(destination.name)}>I want to know more</Button>
                                    <Button variant="contained" color="primary" onClick={() => onDestinationClick(destination)}>Add to my roadtrip</Button>
                                </Box>
                            )}
                        </Box>
                    </ListItem>
                ))}
            </List>
            {visibleCount < popularDestinations.length && (
                <Button variant="text" onClick={showMoreDestinations}>Show More</Button>
            )}
            {visibleCount > 5 && (
                <Button variant="text" onClick={showLessDestinations}>Show Less</Button>
            )}
        </div>
    );
};

export default PopularDestinations;
